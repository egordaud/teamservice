<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Letov
  Date: 5/23/2017
  Time: 9:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<input type="hidden" id="userId" value='<c:out value="${user.id}"/>'></input>
<div class="container center" style="width: 660px;">
    <h3 class="form-signin-heading">Edit user</h3>
    <div class="row">
        <form class="col s12">
            <div class="row">
                <div class="input-field col s6">
                    <input placeholder="Placeholder" value='<c:out value="${user.firstname}"/>' id="first_name" type="text" class="validate">
                    <label for="first_name">First Name</label>
                </div>
                <div class="input-field col s6">
                    <input id="last_name" value="<c:out value="${user.lastname}"/>" type="text" class="validate">
                    <label for="last_name">Last Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" value="<c:out value="${user.email}"/>" class="validate">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input type=text id="position" data-length="20" maxlength="20" value='<c:out value="${user.position}"/>'>
                    <label for="email">Position</label>
                </div>
            </div>
<sec:authorize access="hasAuthority('Admin')">
<div class="row">
                <div class="input-field col s12">
                    <select id="role">
                        <option value="" disabled>Choose user role</option>
                        <option value="0" ${user.rolesById.id == 0 ? "selected" : ""}>Admin</option>
                        <option value="1" ${user.rolesById.id == 1 ? "selected" : ""}>QA</option>
                        <option value="2" ${user.rolesById.id == 2 ? "selected" : ""}>Developer</option>
                    </select>
                    <label>Role</label>
                </div>
            </div>
</sec:authorize>
            <div class="row">
                <div class="input-field col s12">
                    <a class="waves-effect waves-light btn grey darken-4" onclick="SaveUser()" style="width: 98%;">Save</a>
                </div>
            </div>

        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('select').material_select();
    });

    function SaveUser() {
        var role = $('#role').val();
        $.ajax({
            type: "POST",
            url: "/Users/Save?id="+$('#userId').val() + "&email=" + $("#email").val() + "&fn=" + $('#first_name').val() + "&ln=" + $('#last_name').val() + "&p=" + $('#position').val() + "&role=" + role,
            complete: function(res){
                window.location.href = "/Users/All"
            }
        });
    }
</script>
</body>
</html>
