<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Letov
  Date: 5/29/2017
  Time: 3:58 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="container">
<c:forEach items="${model.GetDates()}" var="date">
    <div class="row">
        <div class="col s12">
            <h5><c:out value="${date}"/></h5>
            <table class="bordered highlight centered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Count</th>
                    <th>Task</th>
                    <th>User</th>
                    <sec:authorize access="hasAuthority('Admin') || ${userId} == principal.userID">
                    <th>Delete</th>
                    </sec:authorize>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${model.GetTimes(date)}" var="time">
                    <tr id='<c:out value="${time.id}"/>'>
                        <td>#<c:out value="${time.id}"/></td>
                        <td><c:out value="${time.count}"/></td>
                        <td><a href='javascript:showViewTask(<c:out value="${time.tasksByTaskid.id}"/>)'><c:out value="${time.tasksByTaskid.taskname}"/></a></td>
                        <td><a href='/Users/<c:out value="${time.usersByUserId.id}"/>'><c:out value="${time.usersByUserId.firstname}"/> <c:out value="${time.usersByUserId.lastname}"/></a></td>
                        <sec:authorize access="hasAuthority('Admin') || ${userId} == principal.userID">
                            <td><a style="cursor: pointer;" onclick='DeleteTime(<c:out value="${time.id}"/>)'>Delete</a></td>
                        </sec:authorize>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</c:forEach>
</div>
<div id="viewTaskModal" class="modal modal-fixed-footer">

</div>
</body>
</html>
