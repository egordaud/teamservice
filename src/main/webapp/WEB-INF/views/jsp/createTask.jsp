<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container">
    <div class="section center" style="border:2px solid; padding: 2%;margin-top: 5%;">
        <h4 class="form-signin-heading">Create new Task</h4></br>
        <div class="row center">
            <div class="input-field col s6">
                <i class="material-icons prefix">mode_edit</i>
                <input type="text" name="taskName" id="taskName" required autofocus></br>
                <label for="taskName">Enter Task Name</label>
            </div>
            <div class="input-field col s3 center">
                <select id="taskPriority">
                    <option value="" disabled selected>Choose your option</option>
                    <option value="0" class="green-text">Low</option>
                    <option value="1" class="yellow-text">Normal</option>
                    <option value="2" class="red-text">High</option>
                </select>
                <label style="font-size: 1rem">Select task priority</label>
            </div>
            <div class="input-field col s3 center">
                <select id="taskStage">
                    <option value="" disabled selected>Choose your option</option>
                    <c:forEach items="${Stages}" var="stage">
                        <option value='<c:out value="${stage.id}"/>' ><c:out value="${stage.name}"/></option>
                    </c:forEach>
                </select>
                <label style="font-size: 1rem">Select task stage</label>
            </div>
        </div>
        <div class="row center">
            <div class="input-field col s6">
                <i class="material-icons prefix">mode_edit</i>
                <textarea id="taskBody" name="taskBody" class="materialize-textarea"></textarea>
                <label for="taskBody">Enter Task description</label>
            </div>
            <div class="input-field col s3 center">
                <select id="users" multiple>
                    <option value="" disabled selected>Choose your option</option>
                    <c:forEach items="${Users}" var="user">
                        <option value='<c:out value="${user.id}"/>' ><c:out value="${user.firstname}"/> <c:out value="${user.lastname}"/></option>
                    </c:forEach>
                </select>
                <label style="font-size: 1rem">Select task executors</label>
            </div>
            <div class="input-field col s3 center">
                <input type="number" min="1" max="999" placeholder="Enter task estimate time" id="estimateTime">
            </div>

        </div>
        <div class="row center">
            <div class="input-field col s2"></div>
            <div class="input-field col s4">
            <a class="waves-effect waves-light btn green" onclick="SaveTask();" style="width: 98%;">SAVE TASK</a>
            </div>
            <div class="input-field col s4">
                <a class="waves-effect waves-light btn grey" onclick="goBack();" style="width: 98%;">GO BACK</a>
            </div>
            <div class="input-field col s2"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('select').material_select();
    });
    function goBack() {
        window.history.back();
    }

    function SaveTask() {
        var users = [];
        $('#users :selected').each(function(i, selected){
            users[i] = new Number($(selected).val());
        });

        var model = {
            taskName: $("#taskName").val(),
            taskBody: $("#taskBody").val(),
            taskPriority: new Number($('#taskPriority').val()),
            taskStage: new Number($('#taskStage').val()),
            users: users.slice(1, users.length),
            estimatedTime: new Number($('#estimateTime').val())
        }
        $.ajax({
            type: "POST",
            url: "/Tasks/Save",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            contentType: "application/json",
            mimeType: 'application/json',
            dataType: "json",
            data: JSON.stringify(model),
            complete: function (res) {
                Materialize.toast('Task "' + $("#taskName").val() + "' was created successfully" , 10000);
                window.location.href='/Tasks/All'
            }
        });
    }
</script>