<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Letov
  Date: 5/23/2017
  Time: 9:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="container">
    <div class="section center" style="border:2px solid; padding: 2%;margin-top: 5%;">
        <div class="row center">
            <div class="col s3"><b>First Name:</b> <c:out value="${user.firstname}"/></div>
            <div class="col s3"><b>Last Name:</b> <c:out value="${user.lastname}"/></div>
            <div class="col s3"><b>Email:</b> <c:out value="${user.email}"/></div>
            <div class="col s3"><b>Position:</b> <c:out value="${user.position}"/></div>
        </div>
        <div class="row center">
            <div class="col s3"><b>Role:</b> <c:out value="${user.rolesById.name}"/></div>
            <div class="col s3"><b>Created at:</b> <c:out value="${user.createdat}"/></div>
            <div class="col s3"><b>Updated at:</b> <c:out value="${user.updatedat}"/></div>
            <div class="col s3"><b>Last login at:</b> <c:out value="${user.lastloginat}"/></div>
        </div>
    <div class="row center">
        <sec:authorize access="hasAuthority('Admin') || ${user.id} == principal.userID">
            <div class="col s4 input-field">
                <a class="waves-effect waves-light btn grey" href='/Users/Edit/<c:out value="${user.id}"/>'>EDIT USER</a>
            </div>
        </sec:authorize>
            <div class="col s4 input-field">
                <a class="waves-effect waves-light btn green" href='/Users/<c:out value="${user.id}"/>/Times'>VIEW TIME</a>
            </div>
        <sec:authorize access="hasAuthority('Admin')">
        <div class="col s4 input-field">
                <a class="waves-effect waves-light btn red" onclick='DeleteUser(<c:out value="${user.id}"/>)'>DELETE USER</a>
            </div>
        </sec:authorize>
        </div>
    </div>
</div>
<h4>User Tasks:</h4>
<table class="bordered highlight centered">
    <thead>
    <tr>
        <th>Task id</th>
        <th>Task name</th>
        <th>Task Priority</th>
        <th>Task Stage</th>
        <th>Created at</th>
        <th>Created by</th>
        <th>Total time</th>
        <th>Estimated Time</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${tasks}" var="task">
        <tr id='<c:out value="${task.id}"/>'>
            <td>#<c:out value="${task.id}"/></td>
            <td><a href='javascript:showViewTask(<c:out value="${task.id}"/>)'><c:out value="${task.taskname}"/></a></td>
            <td class='<c:out value="${task.prioritiesByTaskpriority.value == 100 ? 'red-text' : ''}"/>'>
                <b><c:out value="${task.prioritiesByTaskpriority.name}" /></b>
            </td>
            <td><c:out value="${task.stagesByTaskstage.name}"/></td>
            <td><c:out value="${task.createdat}"/></td>
            <td><a href='/Users/<c:out value="${task.usersByCreatedby.id}"/>'><c:out value="${task.usersByCreatedby.firstname}"/> <c:out value="${task.usersByCreatedby.lastname}"/></a></td>
            <td><c:out value="${task.totalTime}"/></td>
            <td><c:out value="${task.estimatedtime}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div id="viewTaskModal" class="modal modal-fixed-footer">

</div>
</body>
</html>
