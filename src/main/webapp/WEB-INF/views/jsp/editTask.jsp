<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Letov
  Date: 5/24/2017
  Time: 9:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal-content">
    <input type="hidden" value='<c:out value="${task.id}"/>' id="taskid">
    <div class="row">
        <div class="input-field col s6">
            <input type="text" name="taskName" id="taskName" required autofocus value='<c:out value="${task.taskname}"/>'></br>
            <label for="taskName">Task Name</label>
        </div>
        <div class="input-field col s2 center">
            <select id="priority">
                <c:forEach items="${Priorities}" var="priority">
                    <option value='<c:out value="${priority.id}"/>' ${task.prioritiesByTaskpriority.id == priority.id ? "selected" : ""}><c:out value="${priority.name}"/></option>
                </c:forEach>
            </select>
            <label for="priority">Task priority</label>
        </div>
        <div class="input-field col s2 center">
            <select id="stage">
            <c:forEach items="${Stages}" var="stage">
                <option value='<c:out value="${stage.id}"/>' ${task.stagesByTaskstage.id == stage.id ? "selected" : ""}><c:out value="${stage.name}"/></option>
            </c:forEach>
            </select>
            <label for="stage">Task stage</label>
        </div>
        <div class="input-field col s2 center">
            <input type="number" min="1" max="999" placeholder="Enter task estimate time" id="estimateTime" value='<c:out value="${task.estimatedtime}"/>'>
            <label for="estimateTime">Estimate time</label>
        </div>
    </div>
    <div class="row">
        <div class="col s8">
            <input type=text id="taskBody" data-length="20" maxlength="20" value='<c:out value="${task.taskdescription}"/>'>
            <label for="taskBody">Task description</label>
        </div>
        <div class="col s4">
            <select id="users" multiple style="width: 100%">
                <c:forEach items="${users}" var="user">
                    <option value='<c:out value="${user.id}"/>' ${usersForTask.contains(user) ? "selected" : ""}><c:out value="${user.firstname}"/> <c:out value="${user.lastname}"/></option>
                </c:forEach>
            </select>
        </div>
    </div>
</div>
<div class="modal-footer">
    <a href="#!" id='saveTaskBtn'  class="modal-action modal-close waves-effect waves-green btn-flat green " onclick="UpdateTask()">Save</a>
    <sec:authorize access="hasAuthority('Admin')">
        <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat red" style="margin-left: 10px;margin-right: 10px;" onclick='DeleteTask(<c:out value="${task.id}"/>)'>Delete</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick='OpenEdit(<c:out value="${task.id}"/>)'>Edit</a>
    </sec:authorize>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat " onclick="CloseModal()">Close</a>
</div>
<script>
    $(document).ready(function() {
        $('select').material_select();
    });

    function UpdateTask() {
        var users = [];
        $('#users :selected').each(function(i, selected){
            users[i] = new Number($(selected).val());
        });

        var model = {
            taskid: new Number($("#taskid").val()),
            taskName: $("#taskName").val(),
            taskBody: $("#taskBody").val(),
            taskPriority: new Number($('#priority').val()),
            taskStage: new Number($('#stage').val()),
            users: users,
            estimatedTime: new Number($('#estimateTime').val())
        }
        $.ajax({
            type: "POST",
            url: "/Tasks/UpdateTask",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            contentType: "application/json",
            mimeType: 'application/json',
            dataType: "json",
            data: JSON.stringify(model),
            complete: function (res) {
                Materialize.toast('Task "' + $("#taskName").val() + "' was updated successfully" , 10000);
                $('#viewTaskModal').load('/Tasks/' + model.taskid);
            }
        });
    }
</script>
