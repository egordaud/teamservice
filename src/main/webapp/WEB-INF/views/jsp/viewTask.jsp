<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="modal-content">
    <h4 style="border-bottom:1 px solid black;">#<c:out value="${task.id}"/> - <c:out value="${task.taskname}"/></h4>
        <div class="row">
            <div class="input-field col s3">
                <b>Task description: </b><br>
                <c:out value="${task.taskdescription}"/>
            </div>
            <div class="input-field col s1 center">
                <b>Priority: </b>
                <span class='<c:out value="${task.prioritiesByTaskpriority.value == 100 ? 'red-text' : ''}"/>'>
                    <b><c:out value="${task.prioritiesByTaskpriority.name}" /></b>
                </span>
            </div>
            <div class="input-field col s2 center">
                <b>Stage: </b>
                <c:out value="${task.stagesByTaskstage.name}"/>
            </div>
            <div class="input-field col s2 center">
                <b>Task time: </b>
                <c:out value="${task.totalTime}"/>/<c:out value="${task.estimatedtime}"/>
            </div>
            <div class="input-field col s2 center">
                <b>Created by: </b>
                <a href='/Users/View/<c:out value="${task.usersByCreatedby.id}"/>'><c:out value="${task.usersByCreatedby.firstname}"/> <c:out value="${task.usersByCreatedby.lastname}"/></a>
                <br><b>Created at: </b>
                <c:out value="${task.createdat}"/>
            </div>
            <div class="input-field col s2 center">
                <b>Updated by: </b>
                <a href='/Users/View/<c:out value="${task.usersByUpdatedby.id}"/>'><c:out value="${task.usersByUpdatedby.firstname}"/> <c:out value="${task.usersByUpdatedby.lastname}"/></a>
                <br><b>Updated at: </b>
                <c:out value="${task.updatedat}"/>
            </div>
        </div>
    <div class="row">
        <div class="input-field col s8">
            <div class="progress left" style="height:35px">
                <div class="determinate" style="height: 35px; width: <c:out value="${progress}"/>%"></div>
            </div>
        </div>
        <div class="input-field col s2">
            <input type="number" min="1" max="999" placeholder="Add time to task" id="time" task-id='<c:out value="${task.id}"/>'>
        </div>
        <div class="input-field col s2">
            <a id='timeButton'  class="waves-effect waves-light btn green" onclick="AddTime()">+ ADD TIME</a>
        </div>
    </div>
    <div class="row">
        <ul class="collapsible" data-collapsible="expandable">
            <li>
                <div class="collapsible-header"><i class="material-icons">account_circle</i>Assigned to Users</div>
                <div class="collapsible-body" style="padding: 0rem 1rem;">
                    <table class="bordered highlight centered">
                        <thead>
                        <tr>
                            <th>User name</th>
                            <th>User position</th>
                            <th>View</th>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach items="${Users}" var="user">
                            <tr>
                                <td><c:out value="${user.firstname}"/> <c:out value="${user.lastname}"/></td>
                                <td><c:out value="${user.position}"/></td>
                                <td><a href='/Users/<c:out value="${user.id}"/>'>View</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </li>
            <li>
                <div class="collapsible-header"><i class="material-icons">textsms</i>Comments</div>
                <div class="collapsible-body" style="padding: 0rem 1rem;">
                    <table class="bordered highlight">
                        <thead>
                        <tr>
                            <th>Comment</th>
                            <th>By</th>
                            <th>Date</th>
                            <sec:authorize access="hasAuthority('Admin')">
                                <th>Delete</th>
                            </sec:authorize>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach items="${Comments}" var="comment">
                            <tr>
                                <td><c:out value="${comment.text}"/></td>
                                <td><a href='/Users/<c:out value="${comment.userByCreatedBy.id}"/>' ><c:out value="${comment.userByCreatedBy.firstname}"/> <c:out value="${comment.userByCreatedBy.lastname}"/></a></td>
                                <td><c:out value="${comment.createdat}"/></td>
                                <sec:authorize access="hasAuthority('Admin')">
                                    <td><a style='cursor: pointer;' onclick='DeleteComment(<c:out value='${comment.id}'/>,<c:out value='${task.id}'/> )'>Delete</a></td>
                                </sec:authorize>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="input-field col s10">
                            <i class="material-icons prefix">mode_edit</i>
                            <input id="comment" style="margin-bottom: 0px;" name="comment" type="text" ></input>
                            <label for="comment">Enter comment</label>
                        </div>
                        <div class="input-field col s2">
                            <a id='addCommentButton'  class="waves-effect waves-light btn green" onclick='AddComment(<c:out value="${task.id}"/>)'>POST</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        </div>
    </div>
</div>
<div class="modal-footer">
    <sec:authorize access="hasAuthority('Admin') || hasAuthority('QA')">
        <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat red" onclick='DeleteTask(<c:out value="${task.id}"/>)'>Delete</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick='OpenEdit(<c:out value="${task.id}"/>)'>Edit</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat green" onclick='DoneTask(<c:out value="${task.id}"/>)'>Done</a>
    </sec:authorize>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat " onclick="CloseModal()">Close</a>
</div>
<script>
    $(document).ready(function(){
        $('.collapsible').collapsible();
    });
</script>