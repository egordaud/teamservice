<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Letov
  Date: 5/23/2017
  Time: 5:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table class="bordered highlight centered">
    <thead>
    <tr>
        <th>Id</th>
        <th>Full Name</th>
        <th>Email</th>
        <th>Position</th>
        <th>Role</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${users}" var="user">
    <tr id='<c:out value="${user.id}"/>'>
        <td>#<c:out value="${user.id}"/></td>
        <td><c:out value="${user.firstname}"/> <c:out value="${user.lastname}"/></td>
        <td><c:out value="${user.email}"/></td>
        <td><c:out value="${user.position}"/></td>
        <td><c:out value="${user.rolesById.name}"/></td>
        <sec:authorize access="hasAuthority('Admin')">
            <td><a href='/Users/<c:out value="${user.id}"/>'>Edit</a></td>
            <td><a style="cursor: pointer;" onclick='DeleteUser(<c:out value="${user.id}"/>)'>Delete</a></td>
        </sec:authorize>
    </tr>
    </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
