<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Spring Security</title>

    <!-- Bootstrap core CSS -->
    <link href="/resources/core/materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container center" style="width: 330px;">
    <c:url value="/j_spring_security_check" var="loginUrl" />
    <form action="${loginUrl}" method="post" id="login_form">
        <h2 class="form-signin-heading">Please sign in</h2>

        <div class="input-field col s6">
            <i class="material-icons prefix">account_circle</i>
            <input type="text" class="validate" name="j_username" id="username" placeholder="Email address" required autofocus>
            <label for="username">Login</label>
        </div>
        <div class="input-field col s6">
            <i class="material-icons prefix">lock</i>
            <input type="password" class="validate" name="j_password" id="password" placeholder="Password" required>
            <label for="password">Password</label>
        </div>
        <div class="input-field col s4" style="padding-left: 10%;">
            <a class="waves-effect waves-light btn grey darken-4" onclick="document.getElementById('login_form').submit();" style="width: 98%;">SIGN IN</a>
        </div>
    </form>

</div>
<script src="/resources/core/jquery/jquery-3.2.1.js"></script>
<script src="/resources/core/materialize/js/materialize.min.js"></script>
</body>
</html>