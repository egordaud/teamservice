<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Letov
  Date: 5/2/2017
  Time: 9:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="container">
    <div class="section">
        <div class="row center">
            <div class="collection">
            <c:forEach items="${Tasks}" var="task">
                <a href='#!' class="collection-item black-text <c:out value="${task.prioritiesByTaskpriority.value == 100 ? 'red accent-2' : ''}"/>" id='<c:out value="${task.id}"/>' onclick="showViewTask(this.id)">
                    <b class="left">#<c:out value="${task.id}"/></b>
                    &nbsp;&nbsp;
                    <b><c:out value="${task.taskname}"/></b>
                    <b><span class="badge "><c:out value="${task.commentsById.size()}"/></span></b></a>
            </c:forEach>
            </div>
        </div>
    </div>
</div>
<!-- Modal Structure -->
<div id="viewTaskModal" class="modal modal-fixed-footer">

</div>
</body>
</html>
