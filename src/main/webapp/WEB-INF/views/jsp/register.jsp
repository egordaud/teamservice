<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Spring Security</title>

    <!-- Bootstrap core CSS -->
    <link href="/resources/core/materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container center" style="width: 660px;">
        <h2 class="form-signin-heading">Create new account</h2>
    <div class="row">
        <form class="col s12">
            <div class="row">
                <div class="input-field col s6">
                    <input placeholder="Placeholder" id="first_name" type="text" class="validate">
                    <label for="first_name">First Name</label>
                </div>
                <div class="input-field col s6">
                    <input id="last_name" type="text" class="validate">
                    <label for="last_name">Last Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="password" type="password" class="validate">
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="confirmPassword" type="password" class="validate">
                    <label for="email">Confirm Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" class="validate">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input type=text id="position" data-length="20" maxlength="20">
                    <label for="email">Position</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <select id="role">
                        <option value="" disabled selected>Choose your role</option>
                        <option value="0">Admin</option>
                        <option value="1">QA</option>
                        <option value="2">Developer</option>
                    </select>
                    <label>Role</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <a class="waves-effect waves-light btn grey darken-4" onclick="RegisterNewUser()" style="width: 98%;">REGISTER</a>
                </div>
            </div>

        </form>
    </div>
</div>
<script src="/resources/core/jquery/jquery-3.2.1.js"></script>
<script src="/resources/core/materialize/js/materialize.min.js"></script>
<script>
    function RegisterNewUser(){
        var role = $('#role').val();
        $.ajax({
            type: "POST",
            url: "/NewUser?email=" + $("#email").val() +"&password=" + $("#password").val() + "&confirm=" + $('#confirmPassword').val() + "&fn=" + $('#first_name').val() + "&ln=" + $('#last_name').val() + "&p=" + $('#position').val() + "&role=" + role,
            complete: function (res) {
                     window.location.href = "/login"
            }
        });
    }

    $(document).ready(function() {
        $('select').material_select();
    });

</script>
</body>
</html>