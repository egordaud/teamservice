<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title><tiles:insertAttribute name="title" ignore="true" /></title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/resources/core/materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/resources/core/materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <script src="/resources/core/jquery/jquery-3.2.1.js"></script>
</head>
<body>

<tiles:insertAttribute name="header" />
<tiles:insertAttribute name="body" />

<script src="/resources/core/materialize/js/materialize.min.js"></script>
<script src="/resources/core/js/init.js"></script>
<script>
    $(document).ready(function(){
        $('#viewTaskModal').modal();
    });

    function showViewTask(id){
        $('#viewTaskModal').load('/Tasks/' + id);
        $('#viewTaskModal').modal('open');
    }

    function CloseModal() {
        $('#viewTaskModal').modal('close');
    }

    function DeleteUser(id) {
        $.ajax({
            type: "POST",
            url: "/Users/Delete/" +id,
            complete: function(res){
                window.location.href = "/Users/All"
            }
        });
    }

    function DeleteTask(id){
        $.ajax({
            url: '/Tasks/Delete/' + id,
            type: 'DELETE',
            complete: function(result) {
                CloseModal();
                Materialize.toast(result.responseText, 10000);
                location.reload();
            }
        });
    }

    function DeleteTime(id){
        $.ajax({
            url: '/Times/Delete/' + id,
            type: 'DELETE',
            complete: function(result) {
                Materialize.toast(result.responseText, 2500);
                location.reload();
            }
        });
    }

    function DoneTask(id){
        $.ajax({
            url: '/Tasks/Done/' + id,
            type: 'DELETE',
            complete: function(result) {
                location.reload();
            }
        });
    }

    function DeleteComment(id, taskId){
        $.ajax({
            type: "DELETE",
            url: "/Tasks/DeleteComment?id="+ id,
            complete: function(res){
                $('#viewTaskModal').load('/Tasks/' + taskId);
            }
        });
    }

    function AddComment(taskId){
        var text = $('#comment').val();
        if(text == undefined) {
            Materialize.toast("Enter the comment value" , 2000);
            return false;
        }
        $.ajax({
            type: "GET",
            url: "/Tasks/AddComment?id="+ taskId +'&text='+ text,
            complete: function(res){
                $('#viewTaskModal').load('/Tasks/' + taskId);
            }
        });
    }

    function AddTime(){
        var taskId = $("#time").attr('task-id');
        var timeCount = $('#time').val();
        if(timeCount == undefined || timeCount == "") {
            Materialize.toast("Enter the time value" , 2000);
            return false;
        }
        $.ajax({
            type: "GET",
            url: "/Tasks/AddTime?id="+ taskId + "&time=" + timeCount,
            success: function(res){
                $('#viewTaskModal').load('/Tasks/' + taskId);
            }
        });
    }

    function OpenEdit(taskId) {
        $('#viewTaskModal').load("/Tasks/Edit/"+ taskId);
        $('#saveTaskBtn').show();
    }
</script>
</body>
</html>