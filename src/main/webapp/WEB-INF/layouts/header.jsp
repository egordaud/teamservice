<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav>
    <div class="nav-wrapper">
        <a href="#!" class="brand-logo left" style="padding-left: 2%">Team Service</a>
        <ul class="right hide-on-med-and-down" style="padding-right: 50px">
            <sec:authorize access="hasAuthority('Admin')">
                <li><a href="/register" class="waves-effect waves-light btn materialize-green">+ Add user</a></li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('Admin') || hasAuthority('QA')">
                <li><a href="/Tasks/Create" class="waves-effect waves-light btn materialize-red">+ Add task</a></li>
            </sec:authorize>
            <li><a href="/Tasks/All">Tasks<span class="new badge"><c:out value="${NewTasksCount}"/></span></a></li>
            <li><a href="/Users/All">Users</a></li>
            <sec:authorize access="hasAuthority('Admin')">
                <li><a href="/Times/All">Times</a></li>
            </sec:authorize>
            <sec:authorize access="hasAuthority('Developer')">
                <li><a href='/Users/<sec:authentication property="principal.userID" />/Times'>My times</a></li>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                <li><a href="<c:url value="/login" />" role="button" class="waves-effect waves-light btn">Login</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href='/Users/<sec:authentication property="principal.userID" />'><sec:authentication property="principal.username" /></a></li>
                <li><a class="waves-effect waves-light btn" href="<c:url value="/logout" />" role="button">Logout</a></li>
            </sec:authorize>
        </ul>
    </div>
</nav>