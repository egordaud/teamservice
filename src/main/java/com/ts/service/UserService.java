package com.ts.service;
import  com.ts.dao.UsersEntity;

public interface UserService {

    UsersEntity getUser(String email);
}
