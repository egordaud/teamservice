package com.ts.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by Letov on 5/2/2017.
 */

public class TSUser extends User
{
    private long userID;

    private String role;

    public TSUser(String username, String password, boolean enabled, boolean accountNonExpired,
                      boolean credentialsNonExpired,
                      boolean accountNonLocked,
                      Collection<? extends GrantedAuthority> authorities, long userID, String role)
    {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.userID = userID;
        this.role = role;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public  boolean isAdmin(){
        return this.role.equals("Admin");
    }

    public  boolean isQA(){
        return this.role.equals("QA");
    }

    public  boolean isDev(){
        return this.role.equals("Developer");
    }


}
