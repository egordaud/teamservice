package com.ts.service;

import com.ts.dao.RolesEntity;
import com.ts.utils.BRUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;
import  com.ts.dao.UsersEntity;
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public TSUser loadUserByUsername(String email) throws UsernameNotFoundException {
        // с помощью нашего сервиса UserService получаем User
        UsersEntity user = userService.getUser(email);
        BRUsers.SetLastLogin(user);
        // указываем роли для этого пользователя
        Set<GrantedAuthority> roles = new HashSet();
        try {
            roles.add(new SimpleGrantedAuthority(user.getRolesById().getName()));
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        // на основании полученныйх даных формируем объект UserDetails
        // который позволит проверить введеный пользователем логин и пароль
        // и уже потом аутентифицировать пользователя
        TSUser tsUser = new TSUser(user.getFirstname()+ " " + user.getLastname(), user.getPassword(), true, true, true,true, roles, user.getId(), user.getRolesById().getName());


        return tsUser;
    }

}