package com.ts.service;

import com.ts.utils.BRUsers;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.ts.utils.HibernateSessionFactory;
import org.hibernate.Session;
import org.hibernate.Criteria;
import  com.ts.dao.UsersEntity;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public UsersEntity getUser(String email) {

        return BRUsers.GetUserByEmail(email);
    }

}