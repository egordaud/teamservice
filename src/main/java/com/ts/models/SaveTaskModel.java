package com.ts.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Letov on 5/2/2017.
 */

public class SaveTaskModel  implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long taskid;
    private String taskName;
    private String taskBody;
    private Long taskStage;
    private Long taskPriority;
    private Long estimatedTime;
    private List<Long> users;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskBody() {
        return taskBody;
    }

    public void setTaskBody(String taskBody) {
        this.taskBody = taskBody;
    }

    public Long getTaskStage() {
        return taskStage;
    }

    public void setTaskStage(Long taskStage) {
        this.taskStage = taskStage;
    }

    public Long getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Long taskPriority) {
        this.taskPriority = taskPriority;
    }

    public Long getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(Long estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public List<Long> getUsers() {
        return users;
    }

    public void setUsers(List<Long> users) {
        this.users = users;
    }

    public Long getTaskid() {
        return taskid;
    }

    public void setTaskid(Long taskid) {
        this.taskid = taskid;
    }
}
