package com.ts.models;

import com.ts.dao.TimeEntity;
import javafx.util.Pair;
import org.springframework.security.ldap.userdetails.Person;

import java.sql.Date;
import java.util.*;

/**
 * Created by Letov on 5/29/2017.
 */
public class TimesModel {

    private HashMap<String, Collection<TimeEntity>> dates;

    public TimesModel(Collection<TimeEntity> times){
        dates = new HashMap<>();
        for(TimeEntity time : times){
            if(dates.containsKey(time.getDatetime().toString())){
                Collection<TimeEntity> today = dates.get(time.getDatetime().toString());
                today.add(time);
            }
            else{
                Collection<TimeEntity> todayTimes = new ArrayList<>();
                todayTimes.add(time);
                dates.put(time.getDatetime().toString(), todayTimes);
            }
        }
    }

    public Collection<TimeEntity> GetTimes(String date){
        return dates.get(date);
    }

    public Collection<String> GetDates(){
        return dates.keySet();
    }
}
