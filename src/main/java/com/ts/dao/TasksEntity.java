package com.ts.dao;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

/**
 * Created by Letov on 5/23/2017.
 */
@Entity
@Table(name = "TASKS", schema = "C##TEAM_SERVICE", catalog = "")
public class TasksEntity {
    private Long id;
    private String taskname;
    private String taskdescription;
    private Long estimatedtime;
    private Date createdat;
    private Date updatedat;
    private Date closedat;
    private Long closedby;
    private Collection<CommentsEntity> commentsById;
    private PrioritiesEntity prioritiesByTaskpriority;
    private StagesEntity stagesByTaskstage;
    private UsersEntity usersByCreatedby;
    private UsersEntity usersByUpdatedby;
    private UsersEntity usersByClosedby;
    private Collection<TimeEntity> timesById;
    private long totalTime;

    private Collection<UserstasksEntity> userstasksById;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, precision = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TASKNAME", nullable = false, length = 40)
    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    @Basic
    @Column(name = "TASKDESCRIPTION", nullable = true, length = 255)
    public String getTaskdescription() {
        return taskdescription;
    }

    public void setTaskdescription(String taskdescription) {
        this.taskdescription = taskdescription;
    }

    @Basic
    @Column(name = "ESTIMATEDTIME", nullable = false, precision = 0)
    public Long getEstimatedtime() {
        return estimatedtime;
    }

    public void setEstimatedtime(Long estimatedtime) {
        this.estimatedtime = estimatedtime;
    }

    @Basic
    @Column(name = "CREATEDAT", nullable = false)
    public Date getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Date createdat) {
        this.createdat = createdat;
    }

    @Basic
    @Column(name = "UPDATEDAT", nullable = true)
    public Date getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(Date updatedat) {
        this.updatedat = updatedat;
    }

    @Basic
    @Column(name = "CLOSEDAT", nullable = true)
    public Date getClosedat() {
        return closedat;
    }

    public void setClosedat(Date closedat) {
        this.closedat = closedat;
    }

    @Basic
    @Column(name = "CLOSEDBY", nullable = true)
    public Long getClosedby() {
        return closedby;
    }

    public void setClosedby(Long closedby) {
        this.closedby = closedby;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TasksEntity that = (TasksEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (taskname != null ? !taskname.equals(that.taskname) : that.taskname != null) return false;
        if (taskdescription != null ? !taskdescription.equals(that.taskdescription) : that.taskdescription != null)
            return false;
        if (estimatedtime != null ? !estimatedtime.equals(that.estimatedtime) : that.estimatedtime != null)
            return false;
        if (createdat != null ? !createdat.equals(that.createdat) : that.createdat != null) return false;
        if (updatedat != null ? !updatedat.equals(that.updatedat) : that.updatedat != null) return false;
        if (closedat != null ? !closedat.equals(that.closedat) : that.closedat != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (taskname != null ? taskname.hashCode() : 0);
        result = 31 * result + (taskdescription != null ? taskdescription.hashCode() : 0);
        result = 31 * result + (estimatedtime != null ? estimatedtime.hashCode() : 0);
        result = 31 * result + (createdat != null ? createdat.hashCode() : 0);
        result = 31 * result + (updatedat != null ? updatedat.hashCode() : 0);
        result = 31 * result + (closedat != null ? closedat.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "tasksByTaskid")
    public Collection<CommentsEntity> getCommentsById() {
        return commentsById;
    }

    public void setCommentsById(Collection<CommentsEntity> commentsById) {
        this.commentsById = commentsById;
    }

    @ManyToOne
    @JoinColumn(name = "TASKPRIORITY", referencedColumnName = "ID", nullable = false)
    public PrioritiesEntity getPrioritiesByTaskpriority() {
        return prioritiesByTaskpriority;
    }

    public void setPrioritiesByTaskpriority(PrioritiesEntity prioritiesByTaskpriority) {
        this.prioritiesByTaskpriority = prioritiesByTaskpriority;
    }

    @ManyToOne
    @JoinColumn(name = "TASKSTAGE", referencedColumnName = "ID", nullable = false)
    public StagesEntity getStagesByTaskstage() {
        return stagesByTaskstage;
    }

    public void setStagesByTaskstage(StagesEntity stagesByTaskstage) {
        this.stagesByTaskstage = stagesByTaskstage;
    }

    @ManyToOne
    @JoinColumn(name = "CREATEDBY", referencedColumnName = "ID", nullable = false)
    public UsersEntity getUsersByCreatedby() {
        return usersByCreatedby;
    }

    public void setUsersByCreatedby(UsersEntity usersByCreatedby) {
        this.usersByCreatedby = usersByCreatedby;
    }

    @ManyToOne
    @JoinColumn(name = "UPDATEDBY", referencedColumnName = "ID")
    public UsersEntity getUsersByUpdatedby() {
        return usersByUpdatedby;
    }

    public void setUsersByUpdatedby(UsersEntity usersByUpdatedby) {
        this.usersByUpdatedby = usersByUpdatedby;
    }

    @ManyToOne
    @JoinColumn(name = "CLOSEDBY", referencedColumnName = "ID", insertable = false, updatable = false)
    public UsersEntity getUsersByClosedby() {
        return usersByClosedby;
    }

    public void setUsersByClosedby(UsersEntity usersByClosedby) {
        this.usersByClosedby = usersByClosedby;
    }

    @OneToMany(mappedBy = "tasksByTaskid")
    public Collection<TimeEntity> getTimesById() {
        return timesById;
    }

    public void setTimesById(Collection<TimeEntity> timesById) {
        this.totalTime = 0;

        if (timesById != null) {
            this.timesById = timesById;
            for (TimeEntity time : this.timesById) {
                totalTime += (time.getCount() == null ? 0 : time.getCount());
            }
        }
    }

    @OneToMany(mappedBy = "tasksByTaskid")
    public Collection<UserstasksEntity> getUserstasksById() {
        return userstasksById;
    }

    public void setUserstasksById(Collection<UserstasksEntity> userstasksById) {
        this.userstasksById = userstasksById;
    }

    @Transient
    public long getTotalTime() {
        return this.totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }
}
