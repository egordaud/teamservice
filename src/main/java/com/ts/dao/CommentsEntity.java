package com.ts.dao;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Letov on 5/23/2017.
 */
@Entity
@Table(name = "COMMENTS", schema = "C##TEAM_SERVICE", catalog = "")
public class CommentsEntity {
    private Long id;
    private Long taskid;
    private String text;
    private Date createdat;
    private Long createdby;
    private TasksEntity tasksByTaskid;
    private UsersEntity userByCreatedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, precision = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TASKID", nullable = false, precision = 0)
    public Long getTaskid() {
        return taskid;
    }

    public void setTaskid(Long taskid) {
        this.taskid = taskid;
    }

    @Basic
    @Column(name = "TEXT", nullable = true, length = 255)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "CREATEDAT", nullable = false)
    public Date getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Date createdat) {
        this.createdat = createdat;
    }

    @Basic
    @Column(name = "CREATEDBY", nullable = false, precision = 0)
    public Long getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentsEntity that = (CommentsEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (taskid != null ? !taskid.equals(that.taskid) : that.taskid != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (createdat != null ? !createdat.equals(that.createdat) : that.createdat != null) return false;
        if (createdby != null ? !createdby.equals(that.createdby) : that.createdby != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (taskid != null ? taskid.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (createdat != null ? createdat.hashCode() : 0);
        result = 31 * result + (createdby != null ? createdby.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "TASKID", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    public TasksEntity getTasksByTaskid() {
        return tasksByTaskid;
    }

    public void setTasksByTaskid(TasksEntity tasksByTaskid) {
        this.tasksByTaskid = tasksByTaskid;
    }

    @ManyToOne
    @JoinColumn(name = "CREATEDBY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    public UsersEntity getUserByCreatedBy() {
        return userByCreatedBy;
    }

    public void setUserByCreatedBy(UsersEntity userByCreatedBy) {
        this.userByCreatedBy = userByCreatedBy;
    }
}
