package com.ts.dao;

import javax.persistence.*;

/**
 * Created by Letov on 5/23/2017.
 */
@Entity
@Table(name = "USERSTASKS", schema = "C##TEAM_SERVICE", catalog = "")
@IdClass(UserstasksEntityPK.class)
public class UserstasksEntity {
    private Long userid;
    private Long taskid;
    private TasksEntity tasksByTaskid;

    @Id
    @Column(name = "USERID", nullable = false, precision = 0)
    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Id
    @Column(name = "TASKID", nullable = false, precision = 0)
    public Long getTaskid() {
        return taskid;
    }

    public void setTaskid(Long taskid) {
        this.taskid = taskid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserstasksEntity that = (UserstasksEntity) o;

        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;
        if (taskid != null ? !taskid.equals(that.taskid) : that.taskid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userid != null ? userid.hashCode() : 0;
        result = 31 * result + (taskid != null ? taskid.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "TASKID", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    public TasksEntity getTasksByTaskid() {
        return tasksByTaskid;
    }

    public void setTasksByTaskid(TasksEntity tasksByTaskid) {
        this.tasksByTaskid = tasksByTaskid;
    }
}
