package com.ts.dao;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Letov on 5/23/2017.
 */
@Entity
@Table(name = "LOGS", schema = "C##TEAM_SERVICE", catalog = "")
public class LogsEntity {
    private Long id;
    private String message;
    private Long type;
    private Date createdat;
    private Long createdby;
    private LogtypeEntity logtypeByType;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, precision = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MESSAGE", nullable = false, length = 255)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "TYPE", nullable = false, precision = 0)
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Basic
    @Column(name = "CREATEDAT", nullable = false)
    public Date getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Date createdat) {
        this.createdat = createdat;
    }

    @Basic
    @Column(name = "CREATEDBY", nullable = false, precision = 0)
    public Long getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogsEntity that = (LogsEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (createdat != null ? !createdat.equals(that.createdat) : that.createdat != null) return false;
        if (createdby != null ? !createdby.equals(that.createdby) : that.createdby != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (createdat != null ? createdat.hashCode() : 0);
        result = 31 * result + (createdby != null ? createdby.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "TYPE", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    public LogtypeEntity getLogtypeByType() {
        return logtypeByType;
    }

    public void setLogtypeByType(LogtypeEntity logtypeByType) {
        this.logtypeByType = logtypeByType;
    }
}
