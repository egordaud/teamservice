package com.ts.dao;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Letov on 5/23/2017.
 */
public class UserstasksEntityPK implements Serializable {
    private Long userid;
    private Long taskid;

    @Column(name = "USERID", nullable = false, precision = 0)
    @Id
    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Column(name = "TASKID", nullable = false, precision = 0)
    @Id
    public Long getTaskid() {
        return taskid;
    }

    public void setTaskid(Long taskid) {
        this.taskid = taskid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserstasksEntityPK that = (UserstasksEntityPK) o;

        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;
        if (taskid != null ? !taskid.equals(that.taskid) : that.taskid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userid != null ? userid.hashCode() : 0;
        result = 31 * result + (taskid != null ? taskid.hashCode() : 0);
        return result;
    }
}
