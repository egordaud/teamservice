package com.ts.dao;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Letov on 5/23/2017.
 */
@Entity
@Table(name = "LOGTYPE", schema = "C##TEAM_SERVICE", catalog = "")
public class LogtypeEntity {
    private Long id;
    private String name;
    private Collection<LogsEntity> logsById;

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "NAME", nullable = false, length = 25)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogtypeEntity that = (LogtypeEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "logtypeByType")
    public Collection<LogsEntity> getLogsById() {
        return logsById;
    }

    public void setLogsById(Collection<LogsEntity> logsById) {
        this.logsById = logsById;
    }
}
