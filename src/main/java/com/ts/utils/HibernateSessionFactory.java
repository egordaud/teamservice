package com.ts.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.io.File;
import  java.util.*;
public class HibernateSessionFactory {

    private static SessionFactory sessionFactory = buildSessionFactory();

    protected static SessionFactory buildSessionFactory() {
        Locale.setDefault(Locale.ENGLISH);
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure().build();
        sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();

        return sessionFactory;
    }


    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public  static Session OpenSession(){
        if(sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }
        try {
            if (getSessionFactory().isOpen()) {
                return getSessionFactory().getCurrentSession();
            } else {
                return getSessionFactory().openSession();
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            return getSessionFactory().openSession();
        }
    }

    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
    }

}
