package com.ts.utils;

import com.ts.dao.RolesEntity;
import com.ts.dao.UserstasksEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import  com.ts.dao.UsersEntity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by Letov on 5/2/2017.
 */

public class BRUsers {

    public static Session session = HibernateSessionFactory.OpenSession();

    public static UsersEntity GetUser(long id) {

        UsersEntity user = null;
        try {
            user = session.get(UsersEntity.class, id);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            return user;
        }
    }

    public static Boolean CreateNewUser(
            String email,
            String password,
            String firstName,
            String lastName,
            String position,
            Long role
    ){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        BRLogs.Write("User with email "+ email + "created succesfully", 0, 0);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        UsersEntity entity = new UsersEntity();
        entity.setEmail(email);
        entity.setPassword(hashedPassword);
        entity.setFirstname(firstName);
        entity.setLastname(lastName);
        entity.setPosition(position);
        entity.setCreatedat(date);
        entity.setRolesById(session.get(RolesEntity.class, role));
        session.save(entity);
        return true;
    }

    public static Boolean UpdateUser(long id, String firstName, String lastName, String email, String position, long role, long updatedBy)
    {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        session.beginTransaction();
        UsersEntity entity = (UsersEntity)session.load(UsersEntity.class,id);
        entity.setRolesById(session.get(RolesEntity.class, role));
        entity.setPosition(position);
        entity.setUpdatedat(date);
        entity.setEmail(email);
        entity.setFirstname(firstName);
        entity.setLastname(lastName);
        session.save(entity);
        session.getTransaction().commit();

        return  true;
    }

    public  static void SetLastLogin(UsersEntity entity){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        session.beginTransaction();
        entity.setLastloginat(date);
        session.update(entity);
        session.getTransaction().commit();
    }

    public  static List<UsersEntity> GetAll(){
        return session.createCriteria(UsersEntity.class).list();
    }

    public static UsersEntity GetUserByEmail(String email) {
        session = HibernateSessionFactory.OpenSession();
        Criteria criteria = session.createCriteria(UsersEntity.class);
        criteria.add(Restrictions.eq("email", email));

        return (UsersEntity)criteria.uniqueResult();
    }

    public static boolean DeleteUser(long id) {
        session.beginTransaction();
        UsersEntity entity = (UsersEntity)session.load(UsersEntity.class,id);
        session.delete(entity);
        session.getTransaction().commit();
        return  true;
    }
}
