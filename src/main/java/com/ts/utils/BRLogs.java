package com.ts.utils;

import org.hibernate.Session;
import com.ts.dao.LogsEntity;
import java.util.Calendar;

/**
 * Created by Letov on 5/3/2017.
 */
public class BRLogs {
    public static Session session = HibernateSessionFactory.OpenSession();

    public static Boolean Write(String text, long userId, long type){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        LogsEntity entity = new LogsEntity();
        entity.setMessage(text);
        entity.setCreatedby(userId);
        entity.setCreatedat(date);
        entity.setType(type);

        session.save(entity);

        return  true;
    }
}
