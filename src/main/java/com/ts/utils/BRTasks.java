package com.ts.utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import  com.ts.dao.*;
import org.hibernate.criterion.Restrictions;

import java.util.*;

/**
 * Created by Letov on 5/2/2017.
 */
public class BRTasks {

    public static Session session = HibernateSessionFactory.OpenSession();

    public static Boolean CreateTask(String name, String desc, long createdBy, long taskStage, long taskPriority, long estimateTime, List<Long> forUsers){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        TasksEntity entity = new TasksEntity();
        entity.setTaskname(name);
        entity.setTaskdescription(desc);
        entity.setPrioritiesByTaskpriority(GetPriority(taskPriority));
        entity.setStagesByTaskstage(GetStage(taskStage));

        entity.setUsersByCreatedby(BRUsers.GetUser(createdBy));
        entity.setCreatedat(date);
        entity.setEstimatedtime(estimateTime);
        session.save(entity);

        session.beginTransaction();

        for(Long forUser : forUsers) {
            UserstasksEntity userTask = new UserstasksEntity();
            userTask.setTaskid(entity.getId());
            userTask.setUserid(forUser);
            session.save(userTask);
        }

        session.getTransaction().commit();

        BRLogs.Write("Task with name '" + name + "' was created succesfully", createdBy, 0);

        return  true;
    }

    public static Boolean updateTask(long id, String name, String desc, long updatedBy, long taskStage, long taskPriority, long estimateTime, List<Long> forUsers){
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        TasksEntity entity = (TasksEntity)session.createCriteria(TasksEntity.class).add(Restrictions.eq("id", id)).uniqueResult();

        entity.setTaskname(name);
        entity.setTaskdescription(desc);
        entity.setPrioritiesByTaskpriority(GetPriority(taskPriority));
        entity.setStagesByTaskstage(GetStage(taskStage));
        entity.setUsersByUpdatedby(BRUsers.GetUser(updatedBy));
        entity.setUpdatedat(date);

        entity.setEstimatedtime(estimateTime);

        session.update(entity);

        session.beginTransaction();
        List<UserstasksEntity> userstasksEntities = session.createCriteria(UserstasksEntity.class)
                .add(Restrictions.eq("taskid", id)).list();

        for(UserstasksEntity userstask : userstasksEntities) {
            session.delete(userstask);
        }

        for(Long forUser : forUsers) {
            UserstasksEntity userTask = new UserstasksEntity();
            userTask.setTaskid(entity.getId());
            userTask.setUserid(forUser);
            session.save(userTask);
        }

        session.getTransaction().commit();

        BRLogs.Write("Task with name '" + name + "' was updated succesfully", updatedBy, 0);

        return  true;
    }

    public static List<TasksEntity> GetAllTasks(){
        return session.createCriteria(TasksEntity.class).list();
    }

    public static List<TasksEntity> GetActiveTasks(){
        Criteria criteria = session.createCriteria(TasksEntity.class);
        criteria.add(Restrictions.isNull("closedat"));
        return criteria.list();
    }

    public static TasksEntity Get(long id){
        TasksEntity entity = (TasksEntity) session.get(TasksEntity.class, id);
        session.refresh(entity);
        return entity;
    }

    public static long GetAllTaskCount(){
        return session.createCriteria( TasksEntity.class).list().size();
    }

    public static String Delete(long id, long byUser) {
        TasksEntity entity = Get(id);
        session.beginTransaction();
        session.delete(entity);
        session.getTransaction().commit();

        String textMessage = "Task with name '" + entity.getTaskname() + "' was deleted succesfully";
        BRLogs.Write(textMessage, byUser, 0);

        return textMessage;
    }

    public static String DeleteTime(long id, long byUser) {
        TimeEntity entity = session.get(TimeEntity.class, id);
        session.beginTransaction();
        session.delete(entity);
        session.getTransaction().commit();

        String textMessage = "Time with id '" + entity.getId() + "' was deleted succesfully";
        BRLogs.Write(textMessage, byUser, 0);

        return textMessage;
    }

    public static List<StagesEntity> GetStages() {
        return session.createCriteria(StagesEntity.class).list();
    }

    public static List<PrioritiesEntity> GetPriorities() {
        return session.createCriteria(PrioritiesEntity.class).list();
    }

    private static StagesEntity GetStage(long id){
        return session.get(StagesEntity.class, id);
    }

    private static PrioritiesEntity GetPriority(long id){
        return session.get(PrioritiesEntity.class, id);
    }

    public static long GetTotalTimeForTask(long taskId){
        Criteria criteria = session.createCriteria(TimeEntity.class);
        criteria.add(Restrictions.eq("taskid", taskId));
        Collection<TimeEntity> times =  criteria.list();

        long totalTime = 0;

        for(TimeEntity time : times){
            totalTime += time.getCount();
        }

        return totalTime;
    }
    public static void AddTime(long taskId, long userID, long timeCount, String fullName) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());

        TimeEntity entity = new TimeEntity();
        entity.setCount(timeCount);
        entity.setDatetime(date);
        entity.setTasksByTaskid(BRTasks.Get(taskId));
        entity.setUserid(userID);
        session.save(entity);

        String textMessage = "User "+ fullName + " add time (" + timeCount + ") to task #" + taskId;
        BRLogs.Write(textMessage, userID, 0);
    }

    public static TasksEntity GetTask(long taskId){
        return session.get(TasksEntity.class, taskId);
    }

    public static List<TasksEntity> GetUserTasks(long userId) {
        Criteria criteria = session.createCriteria(UserstasksEntity.class);
        criteria.add(Restrictions.eq("userid", userId));
        Collection<UserstasksEntity> usersTasks =  criteria.list();
        List<TasksEntity> tasks = new ArrayList<TasksEntity>();
        for(UserstasksEntity userTask : usersTasks){
            tasks.add(BRTasks.GetTask(userTask.getTaskid()));
        }

        return  tasks;
    }

    public static List<UsersEntity> GetUsersForTask(Long taskId) {
        Criteria criteria = session.createCriteria(UserstasksEntity.class);
        criteria.add(Restrictions.eq("taskid", taskId));
        Collection<UserstasksEntity> usersTasks =  criteria.list();
        List<UsersEntity> users = new ArrayList<UsersEntity>();
        for(UserstasksEntity userTask : usersTasks){
            users.add(BRUsers.GetUser(userTask.getUserid()));
        }

        return  users;
    }

    public static void AddComment(long id, String text, long userID) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        CommentsEntity entity = new CommentsEntity();
        entity.setText(text);
        entity.setCreatedat(date);
        entity.setCreatedby(userID);
        entity.setTaskid(id);
        session.save(entity);
    }

    public static List<CommentsEntity> GetComments(long id) {
        session = HibernateSessionFactory.OpenSession();
        Criteria criteria = session.createCriteria(CommentsEntity.class);
        criteria.add(Restrictions.eq("taskid", id));
        return criteria.list();
    }

    public static Boolean DeleteComment(long id) {
        CommentsEntity entity = session.get(CommentsEntity.class, id);
        session.beginTransaction();
        session.delete(entity);
        session.getTransaction().commit();

        return  true;
    }

    public static List GetAllTimes(){
        return session.createCriteria( TimeEntity.class).list();
    }

    public static Collection<TimeEntity> GetUserTimes(long id){
        session = HibernateSessionFactory.OpenSession();
        Criteria criteria = session.createCriteria(TimeEntity.class);
        criteria.add(Restrictions.eq("userid", id));
        return (Collection<TimeEntity>)criteria.list();
    }

    public static Boolean DoneTask(long id, long userID) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        TasksEntity entity = session.get(TasksEntity.class, id);
        entity.setClosedat(date);
        entity.setClosedby(userID);
        entity.setStagesByTaskstage(GetStage(7));
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
        return true;
    }
}
