package com.ts.web;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.ts.dao.TasksEntity;
import com.ts.dao.TimeEntity;
import com.ts.dao.UsersEntity;
import com.ts.models.SaveTaskModel;
import com.ts.models.TimesModel;
import com.ts.service.TSUser;
import com.ts.utils.BRTasks;
import com.ts.utils.BRUsers;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Letov on 5/23/2017.
 */
@Controller
@RequestMapping("/Users")
@PreAuthorize("isAuthenticated()")
public class UsersController {

    @RequestMapping(value = "/All", method = RequestMethod.GET, produces = "application/json")
    public String viewUsers(HttpServletRequest request, HttpServletResponse response)
    {
        List<UsersEntity> users = BRUsers.GetAll();
        request.setAttribute("users", users);

        return "viewUsers";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public String viewUser(@PathVariable("id") long Id,  HttpServletRequest request, HttpServletResponse response)
    {
        UsersEntity user = BRUsers.GetUser(Id);
        List<TasksEntity> tasks = BRTasks.GetUserTasks(Id);
        request.setAttribute("user", user);
        request.setAttribute("tasks", tasks);
        return "viewUser";
    }

    @RequestMapping(value = "/Delete/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> deleteUser(@PathVariable("id") long Id,  HttpServletRequest request, HttpServletResponse response)
    {
        Boolean bool = BRUsers.DeleteUser(Id);

        ResponseEntity<String> responseEntity = new ResponseEntity<>("{success: true}",
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/Edit/{id}", method = RequestMethod.GET, produces = "application/json")
    public String editUser(@PathVariable("id") long Id,  HttpServletRequest request, HttpServletResponse response)
    {
        UsersEntity user = BRUsers.GetUser(Id);

        request.setAttribute("user", user);
        return "editUser";
    }

    @RequestMapping(value = "/Save", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> saveUser(
            @RequestParam("id") long id,
            @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "fn", required = true) String firstName,
            @RequestParam(value = "ln", required = true) String lastName,
            @RequestParam(value = "p", required = true) String position,
            @RequestParam(value = "role", required = true) Long role,
            HttpServletRequest request,
            HttpServletResponse response
    )
    {
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Boolean result = BRUsers.UpdateUser(
                id,
                firstName,
                lastName,
                email,
                position,
                role,
                userDetails.getUserID()
        );
        ResponseEntity<String> responseEntity = new ResponseEntity<>("{success: true}",
                HttpStatus.OK);

        return responseEntity;
    }

    @RequestMapping(value = "/{id}/Times", method = RequestMethod.GET, produces = "application/json")
    public String viewTasks(@PathVariable("id") long Id, HttpServletRequest request, HttpServletResponse response)
    {
        TimesModel model = new TimesModel(BRTasks.GetUserTimes(Id));
        request.setAttribute("model", model);
        request.setAttribute("userId", Id);
        return "viewTimes";}
}