package com.ts.web.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.tiles.Attribute;
import org.apache.tiles.Definition;
import org.apache.tiles.definition.DefinitionsFactory;
import org.apache.tiles.request.Request;

/**
 * @author Bhimu
 *
 * <code>Apache tiles configuration class. Implements DefinitionsFactory to provide programmatic configuration for Apache tiles.</code>
 *
 */
public final class TilesDefinitionsConfig implements DefinitionsFactory {

    private static final Map<String, Definition> tilesDefinitions = new HashMap<String,Definition>();
    private static final Attribute BASE_TEMPLATE = new Attribute("/WEB-INF/layouts/layout.jsp");
    private static final Attribute EMPTY_TEMPLATE = new Attribute("/WEB-INF/layouts/emptylayout.jsp");

    @Override
    public Definition getDefinition(String name, Request tilesContext) {
        return tilesDefinitions.get(name);
    }

    /**
     * @param name <code>Name of the view</code>
     * @param title <code>Page title</code>
     * @param body <code>Body JSP file path</code>
     *
     * <code>Adds default layout definitions</code>
     */
    private static void addDefaultLayoutDef(String name, String title, String body, Boolean hasLayout) {
        Map<String, Attribute> attributes = new HashMap<String,Attribute>();
        if(hasLayout) {
            attributes.put("title", new Attribute(title));
            attributes.put("header", new Attribute("/WEB-INF/layouts/header.jsp"));
            attributes.put("body", new Attribute("/WEB-INF/views/jsp/" + body));
            tilesDefinitions.put(name, new Definition(name, BASE_TEMPLATE, attributes));
        }
        else{
            attributes.put("body", new Attribute("/WEB-INF/views/jsp/" + body));
            attributes.put("title", new Attribute(title));
            tilesDefinitions.put(name, new Definition(name, EMPTY_TEMPLATE, attributes));
        }

    }

    /**
     * <code>Add Apache tiles definitions</code>
     */
    public static void addDefinitions(){
        addDefaultLayoutDef("index", "Index", "index.jsp", true);
        addDefaultLayoutDef("login", "Login", "login.jsp", false);
        addDefaultLayoutDef("register", "Register", "register.jsp", true);
        addDefaultLayoutDef("createTask", "Create new task", "createTask.jsp", true);
        addDefaultLayoutDef("viewTasks", "All tasks", "viewTasks.jsp", true);
        addDefaultLayoutDef("viewTask", "View Task", "viewTask.jsp", false);
        addDefaultLayoutDef("viewUsers", "View Users", "viewUsers.jsp", true);
        addDefaultLayoutDef("editUser", "Edit User", "editUser.jsp", true);
        addDefaultLayoutDef("viewUser", "View User", "viewUser.jsp", true);
        addDefaultLayoutDef("editUser", "Edit User", "editUser.jsp", true);
        addDefaultLayoutDef("editTask", "Edit Task", "editTask.jsp", false);
        addDefaultLayoutDef("viewTimes", "View Times", "viewTimes.jsp", true);
    }
}