package com.ts.web;

import com.ts.dao.UsersEntity;
import com.ts.service.TSUser;
import com.ts.utils.BRTasks;
import com.ts.utils.BRUsers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class MainController {

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerPage(Model model)
    {
        return "register";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String start(HttpServletRequest request, HttpServletResponse response){
        return "login";
    }

    @RequestMapping(value="/j_spring_security_check", method = RequestMethod.POST)
    public String securityCheck(HttpServletRequest request, HttpServletResponse response){
        return "index";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/NewUser", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> newUser(
            @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "password", required = true) String password,
            @RequestParam(value = "confirm", required = true) String confirm,
            @RequestParam(value = "fn", required = true) String firstName,
            @RequestParam(value = "ln", required = true) String lastName,
            @RequestParam(value = "p", required = true) String position,
            @RequestParam(value = "role", required = true) Long role)
            throws IOException
    {
        Boolean result = false;
        if(true) {
            result = BRUsers.CreateNewUser(
                    email,
                    password,
                    firstName,
                    lastName,
                    position,
                    role
            );
        }

        ResponseEntity<String> responseEntity = new ResponseEntity<>("{success: " + result +" }",
                HttpStatus.OK);
        return responseEntity;
    }
}