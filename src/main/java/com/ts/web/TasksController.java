package com.ts.web;

import com.ts.models.SaveTaskModel;
import com.ts.service.TSUser;
import com.ts.utils.BRTasks;
import com.ts.utils.BRUsers;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import  com.ts.dao.*;

@Controller
@RequestMapping("/Tasks")
@PreAuthorize("isAuthenticated()")
public class TasksController {

    @RequestMapping(value = "/Create", method = RequestMethod.GET)
    public String createTask(HttpServletRequest request, HttpServletResponse response)
    {
        request.setAttribute("Users", BRUsers.GetAll());
        request.setAttribute("Stages", BRTasks.GetStages());
        return "createTask";
    }

    @RequestMapping(value = "/All", method = RequestMethod.GET, produces = "application/json")
    public String viewTasks(HttpServletRequest request, HttpServletResponse response)
    {
        List<TasksEntity> tasks = BRTasks.GetActiveTasks();
        request.setAttribute("Tasks", tasks);
        request.getSession().setAttribute("NewTasksCount", BRTasks.GetAllTaskCount());
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        request.getSession().setAttribute("userid", userDetails.getUserID());
        return "viewTasks";
    }

    @RequestMapping("/{id}")
    public String viewTask(@PathVariable("id") long id, HttpServletRequest request) {
        TasksEntity entity = BRTasks.Get(id);
        List<UsersEntity> users = BRTasks.GetUsersForTask(entity.getId());
        double percentage = (double)entity.getTotalTime() / (double)entity.getEstimatedtime();
        long progress = (long)(percentage * 100);

        List<CommentsEntity> comments = BRTasks.GetComments(id);
        request.setAttribute("task", entity);
        request.setAttribute("progress", progress);
        request.setAttribute("Users", users);
        request.setAttribute("Comments", comments);
        return "viewTask";
    }

    @RequestMapping("/Edit/{id}")
    public String editTask(@PathVariable("id") long id, HttpServletRequest request) {
        TasksEntity entity = BRTasks.Get(id);
        List<UsersEntity> usersForTask = BRTasks.GetUsersForTask(entity.getId());
        List<UsersEntity> users = BRUsers.GetAll();
        request.setAttribute("task", entity);
        request.setAttribute("users", users);
        request.setAttribute("usersForTask", usersForTask);
        request.setAttribute("Stages", BRTasks.GetStages());
        request.setAttribute("Priorities", BRTasks.GetPriorities());
        return "editTask";
    }

    @RequestMapping(value = "/Save", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> saveTask(@RequestBody String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SaveTaskModel model = mapper.readValue(json, SaveTaskModel.class);

        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = userDetails.getUsername();
        UsersEntity user = BRUsers.GetUser(userDetails.getUserID());

        BRTasks.CreateTask(model.getTaskName(), model.getTaskBody(), userDetails.getUserID(), model.getTaskStage(), model.getTaskPriority(),  model.getEstimatedTime(), model.getUsers());

        ResponseEntity<String> responseEntity = new ResponseEntity<>("{success: true}",
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/UpdateTask", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> updateTask(@RequestBody String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SaveTaskModel model = mapper.readValue(json, SaveTaskModel.class);

        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = userDetails.getUsername();
        UsersEntity user = BRUsers.GetUser(userDetails.getUserID());

        BRTasks.updateTask(model.getTaskid(), model.getTaskName(), model.getTaskBody(), userDetails.getUserID(), model.getTaskStage(), model.getTaskPriority(),  model.getEstimatedTime(), model.getUsers());

        ResponseEntity<String> responseEntity = new ResponseEntity<>("{success: true}",
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/AddTime",  method = RequestMethod.GET)
    public ResponseEntity<String> addTime(
            @RequestParam("id") long id,
            @RequestParam("time") long timeCount,
            HttpServletRequest request
    )
    {
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        BRTasks.AddTime(id, userDetails.getUserID(), timeCount, userDetails.getUsername());

        ResponseEntity<String> responseEntity = new ResponseEntity<String>("TRUE",
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/AddComment",  method = RequestMethod.GET)
    public ResponseEntity<String> addTime(
            @RequestParam("id") long id,
            @RequestParam("text") String text,
            HttpServletRequest request
    )
    {
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        BRTasks.AddComment(id, text, userDetails.getUserID());
        ResponseEntity<String> responseEntity = new ResponseEntity<String>("TRUE",
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/Delete/{id}",  method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTask(@PathVariable("id") long id, HttpServletRequest request) {
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String message = BRTasks.Delete(id, userDetails.getUserID());

        ResponseEntity<String> responseEntity = new ResponseEntity<>(message,
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/DeleteComment",  method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteComment(@RequestParam("id") long id, HttpServletRequest request) {
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Boolean message = BRTasks.DeleteComment(id);

        ResponseEntity<String> responseEntity = new ResponseEntity<>(message.toString(),
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/Done/{id}",  method = RequestMethod.DELETE)
    public ResponseEntity<String> doneTask(@PathVariable("id") long id, HttpServletRequest request) {
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Boolean message = BRTasks.DoneTask(id, userDetails.getUserID());

        ResponseEntity<String> responseEntity = new ResponseEntity<>(message.toString(),
                HttpStatus.OK);
        return responseEntity;
    }
}