package com.ts.web;

import com.ts.dao.TasksEntity;
import com.ts.dao.UsersEntity;
import com.ts.models.TimesModel;
import com.ts.service.TSUser;
import com.ts.utils.BRTasks;
import com.ts.utils.BRUsers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;

/**
 * Created by Letov on 5/29/2017.
 */

@Controller
@RequestMapping("/Times")
@PreAuthorize("isAuthenticated()")
public class TimesController {

    @RequestMapping(value = "/All", method = RequestMethod.GET, produces = "application/json")
    public String viewUsers(HttpServletRequest request, HttpServletResponse response) {
        TimesModel model = new TimesModel(BRTasks.GetAllTimes());
        request.setAttribute("isAdmin", request.isUserInRole("Admin"));
        request.setAttribute("model", model);
        request.setAttribute("userId", -1);
        return "viewTimes";
    }

    @RequestMapping(value = "/Delete/{id}",  method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTask(@PathVariable("id") long id, HttpServletRequest request) {
        TSUser userDetails = (TSUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String message = BRTasks.DeleteTime(id, userDetails.getUserID());

        ResponseEntity<String> responseEntity = new ResponseEntity<>(message,
                HttpStatus.OK);
        return responseEntity;
    }
}
